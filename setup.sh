#!/usr/bin/env sh

mkdir -pv $HOME/workspace/{notes,works,projects,tutorials}
case "$OSTYPE" in
  darwin*)
	  curl -s https://codeberg.org/4nzar/dotfiles/raw/branch/main/.macos | sh
	  ;; 
#  linux*)
#	  echo "LINUX"
#	  mkdir $HOME/workspace/{notes,works,projects,tutorials}
#	  curl https://codeberg.org/4nzar/dotfiles/raw/branch/main/setup/linux
#	  ;;
#  bsd*)
#	  echo "BSD"
#	  mkdir $HOME/workspace/{notes,works,projects,tutorials}
#	  curl https://codeberg.org/4nzar/dotfiles/raw/branch/main/setup/bsd
#	  ;;
#  solaris*)
#	  echo "SOLARIS"
#	  mkdir $HOME/workspace/{notes,works,projects,tutorials}
#	  ;;
#  msys*)
#	  echo "WINDOWS"
#	  ;;
  *)
	  echo "unknown: $OSTYPE"
	  ;;
esac
sh $HOME/.config/git/setup
